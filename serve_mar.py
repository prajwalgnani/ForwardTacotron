#!/usr/bin/env python3

import torch

from models.forward_tacotron import ForwardTacotron
from utils import hparams as hp
from utils.text.symbols import phonemes
from utils.paths import Paths
import argparse
from utils.text import text_to_sequence, sequence_to_text, clean_text
from utils.display import simple_table
from utils.dsp import reconstruct_waveform, save_wav

import os
import glob
import tqdm
import time
import random
import string
import numpy as np
import json

from scipy.io.wavfile import write

try:
    from melgan_model.generator import Generator
except ImportError:
    print("Run ln -s //path/to/melgan//model //path/to/ForwardTacotron//melgan_model")
try:
    from melgan_utils.hparams import HParam, load_hparam_str
except ImportError:
    print("Run ln -s //path/to/melgan//utils //path/to/ForwardTacotron//melgan_utils")


class TTS:
    def __init__(self, melgan_checkpoint_path, melgan_config, hp):
        self.melgan_checkpoint_path = melgan_checkpoint_path
        self.melgan_config = melgan_config
        tts_weights = None  # Check line 72
        self.alpha = 1.0
        vocoder = "melgan"
        force_cpu = False
        model_load_start = time.time()

        self.paths = Paths(hp.data_path, hp.voc_model_id, hp.tts_model_id)

        if not force_cpu and torch.cuda.is_available():
            device = torch.device("cuda:0")
        else:
            device = torch.device("cpu")
        print("Using device:", device)

        print("\nInitialising Forward TTS Model...\n")
        self.tts_model = ForwardTacotron(
            embed_dims=hp.forward_embed_dims,
            num_chars=len(phonemes),
            durpred_rnn_dims=hp.forward_durpred_rnn_dims,
            durpred_conv_dims=hp.forward_durpred_conv_dims,
            durpred_dropout=hp.forward_durpred_dropout,
            rnn_dim=hp.forward_rnn_dims,
            postnet_k=hp.forward_postnet_K,
            postnet_dims=hp.forward_postnet_dims,
            prenet_k=hp.forward_prenet_K,
            prenet_dims=hp.forward_prenet_dims,
            highways=hp.forward_num_highways,
            dropout=hp.forward_dropout,
            n_mels=hp.num_mels,
        ).to(device)

        tts_load_path = (
            tts_weights if tts_weights else self.paths.forward_latest_weights_mar
        )
        self.tts_model.load(tts_load_path)

        self.tts_k = self.tts_model.get_step() // 1000
        simple_table(
            [("Forward Tacotron", str(self.tts_k) + "k"), ("Vocoder Type", "MelGAN")]
        )

        print("\n Initialising Melgan Vocoder Model...\n")
        checkpoint = torch.load(self.melgan_checkpoint_path)
        if self.melgan_config is not None:
            mhp = HParam(self.melgan_config)
        else:
            mhp = load_hparam_str(checkpoint["hp_str"])

        self.output_sample_rate = mhp.audio.sampling_rate
        self.model = Generator(mhp.audio.n_mel_channels).cuda()
        self.model.load_state_dict(checkpoint["model_g"])
        self.model.eval(inference=False)
        model_load_stop = time.time()
        model_load_benchmark = model_load_stop - model_load_start
        print("Model loaded in: {} seconds".format(model_load_benchmark))

    def synthesize(self, text):
        inf_time_start = time.time()
        id = "".join(random.choices(string.ascii_uppercase + string.digits, k=5))
        inputs = clean_text(text.strip(), lang="hi", grapheme=False)
        inputs = text_to_sequence(inputs)
        # print("input set", len(x))
        phoneseq = sequence_to_text(inputs)
        phoneme_sequence = sequence_to_text(inputs).split()
        # phoneme_sequence = in_inputs[i].split()
        grapheme_sequence = text.split()
        phgr = True
        op, m, dur = self.tts_model.generate(inputs, alpha=self.alpha)
        if len(phoneme_sequence) != len(grapheme_sequence):
            print(
                "WARNING::: phoneme sequence({}) is not equal to grapheme sequence({})".format(
                    len(phoneme_sequence), len(grapheme_sequence)
                )
            )
            phgr = False

        dur_hop = dur * 256
        dur_sample = dur_hop / 22050
        csdur = np.cumsum(dur_sample)
        correlation = []
        for ind, pho in enumerate(csdur):
            phonem = phoneseq[ind]
            correlation.append(
                {"phoneme": phonem, "time": str(pho),}
            )
        # pprint(correlation)
        alignment_path = (
            self.paths.forward_output
            / f"{id}_{self.tts_k}_alpha{self.alpha}_phone.json"
        )
        with open(alignment_path, "w+", encoding="utf-8",) as jf:
            json.dump(correlation, jf, ensure_ascii=False)

        mel = torch.tensor(m).unsqueeze(0)
        mel_save_path = (
            self.paths.forward_output / f"{id}_{self.tts_k}_alpha{self.alpha}.mel"
        )
        torch.save(m, mel_save_path)

        if len(mel.shape) == 2:
            mel = mel.unsqueeze(0)
        mel = mel.cuda()

        audio = self.model.inference(mel)
        audio = audio.cpu().detach().numpy()

        out_path = (
            self.paths.forward_output / f"{id}_{self.tts_k}_alpha{self.alpha}.wav"
        )
        write(out_path, self.output_sample_rate, audio)
        inf_time_stop = time.time()
        inf_time_benchmark = inf_time_stop - inf_time_start
        print("Inferred in {} seconds".format(inf_time_benchmark))
        # return (inf_time_benchmark, str(out_path), str(alignment_path), correlation)
        return {
            "inference_time": inf_time_benchmark,
            "result_path": str(out_path),
            "alignment_file": str(alignment_path),
            "alignments": correlation,
            "text": text,
            "phones": phoneseq,
        }


if __name__ == "__main__":

    melgan_config = "/home/ubuntu/projects/melgan_trim/config/config.yaml"
    melgan_checkpoint_path = "/home/ubuntu/projects/melgan_trim/chkpt/hin_fem_trim/hin_fem_trim_aca5990_4800.pt"
    text = "आपके बहुमूल्य समय के लिए धन्यवाद। आपका दिन शुभ हो"
    text = text + ". a"
    # text = text + "."
    hp_file = "/home/ubuntu/projects/ForwardTacotron/hparams.py"
    hp.configure(hp_file)

    tts = TTS(melgan_checkpoint_path, melgan_config, hp)
    audio_path = tts.synthesize(text)
    print("File Created at audio path: ", audio_path)
